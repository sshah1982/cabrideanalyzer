Instructions to run Project:

The program is in Java (Hadoop Map Reduce Framework)

(1) You can run either from command prompt or from Eclipse. 

(2) To run from command prompt, download CabRideAnalyzer.jar and provide two parameters 
separated by space.

For this option to work, JAVA_HOME environment variable should be set and JDK/bin should be
part of the PATH environment variable. 

First is valid path of the CSV and second is output directory path. 
Output directory MUST NOT exist beforehand on the system otherwise program would throw Exception.

java -jar CabRideAnalyzer.jar D://Cab_Ride_Data.csv D://Output

(3) To run from Eclipse, Go to the CabRideOrderBy.java and select Run as => Run Configuration.
in the Arguments tab, provide two parameters separated by space and select Apply and Run.

D://Cab_Ride_Data.csv D://Output

As described above, output directory MUST NOT exist beforehand.

(4) Output would be generated in the given directory. It would have two files. 
_SUCCESS (Empty file)
part-r-00000 (Actual Data File)

(5) For containerization, Docker can be used.

(6) As of now, there is only one Reducer in the program. If we want to scale to 100 million 
or more entries, using combiner is a good approach.

job.setCombinerClass(CabRideAggregationReducer.class);

Combiners largely reduces the number of entries going as input into reducers, so load on 
reduce tasks becomes very less.

Also using partitioner makes sure that load on any mapper and reducer does not become uneven.
Partitioner is already used in the program.

Another approach is to use Spark instead of Hadoop which can use in memory feature for sorting 
large datasets.

(7) Design of the solution(How it works)

The requirement is that we have to aggregate records based on origin, destination, datetime and 
hour of the day.

For this, I've created composite key of these 4 fields which implements WritableComparable 
interface.

Based on this key, sorting and aggregation takes place.

I've also made sure that bad records get dropped in the map phase itself and do not reach 
reduce phase at all.

The program considers first line as header which won't be part of the output.















