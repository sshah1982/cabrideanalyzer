package com.xyz.util;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class CabRideFullKeyComparator extends WritableComparator  {
	
	public CabRideFullKeyComparator() {
		super(CabRideCompositeKey.class, true);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable wc1, WritableComparable wc2) {

		CabRideCompositeKey key1 = (CabRideCompositeKey) wc1;
		CabRideCompositeKey key2 = (CabRideCompositeKey) wc2;

		int originCmp = key1.getOrigin().toLowerCase().compareTo(key2.getOrigin().toLowerCase());
		if (originCmp != 0) {
			return originCmp;
		} else {
			int destCmp = key1.getDest().toLowerCase().compareTo(key2.getDest().toLowerCase());
			if (destCmp != 0) {
				return destCmp;
			} else {
				int dateTimeCmp = key1.getDateTime().toLowerCase().compareTo(key2.getDateTime().toLowerCase());
				if(dateTimeCmp != 0) {
					return dateTimeCmp;
				}
				else {
					return Integer.compare(key1.getHourOfTheDay(), key2.getHourOfTheDay());
				}
			}
		}

	}

}
