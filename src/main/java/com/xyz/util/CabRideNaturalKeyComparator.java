package com.xyz.util;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class CabRideNaturalKeyComparator extends WritableComparator  {

	public CabRideNaturalKeyComparator() {
		super(CabRideCompositeKey.class, true);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable wc1, WritableComparable wc2) {

		CabRideCompositeKey key1 = (CabRideCompositeKey) wc1;
		CabRideCompositeKey key2 = (CabRideCompositeKey) wc2;
		
		return key1.getOrigin().compareTo(key2.getOrigin());

	}

}
