package com.xyz.util;

public class CabRideDataParser {
	
	//Here we assume that each record has ALWAYS 5 fields
	private static String[] parsedData = new String[5];
	
	public static void parseData(String csvLine) {
		parsedData = csvLine.split(",");
	}
	
	/* This method checks for bad records.
	If a CSV record doesn't have 5 fields and any of them is null or blank, method returns true.
	If a record has 5 fields and all are Not Null, it returns false.
	This method is required so that we can drop bad records in the Map phase itself and they 
	do not reach Reduce phase. It would decrease load on Reducer. */
	public static boolean isBadRecord() {
		for(int cnt = 0; cnt < 5; cnt++) {
			if(parsedData[cnt] == null || parsedData[cnt].trim().equals("")) {
				return true;
			}
		}
		
		return false;
	}
	
	public static String getRegion() {
		return parsedData[0].trim();
	}
	
	public static String[] getOriginOrDest(String type) {
		String[] latLon = new String[3];
		if(type.equalsIgnoreCase("Origin")) {
			latLon = parsedData[1].trim().split(" ");
		}
		else {
			latLon = parsedData[2].trim().split(" ");
		}
		
		return latLon;
	}
	
	public static String getDateTime() {
		return parsedData[3].trim();
	}
	
	public static String getDataSource() {
		return parsedData[4].trim();
	}
	
	//Hour is in 24 hour format
	public static int getHourOfTheDay() {
		String[] dateTimeData = parsedData[3].split(" ");
		String[] timeData = dateTimeData[1].trim().split(":");
		
		return Integer.parseInt(timeData[0].trim());
	}
	
	

}
