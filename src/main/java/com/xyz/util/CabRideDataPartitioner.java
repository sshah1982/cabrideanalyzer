package com.xyz.util;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class CabRideDataPartitioner extends Partitioner<CabRideCompositeKey, Text> {

	@Override
	public int getPartition(CabRideCompositeKey key, Text value, int numPartitions) {

		// Automatic n-partitioning using hash on the Origin
		return Math.abs(key.getOrigin().hashCode() & Integer.MAX_VALUE) % numPartitions;
	}
	
}