package com.xyz.util;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

public class CabRideCompositeKey implements WritableComparable<CabRideCompositeKey> {

	private String origin; //Is in POINT(Latitude, Longitude)
	private String dest;   //Is in POINT(Latitude, Longitude)
	private String dateTime; //Is DateTime
	private int hourOfTheDay; //Extracted from DateTime

	public CabRideCompositeKey() {
	}

	public CabRideCompositeKey(String origin, String dest, String dateTime, int hourOfTheDay) {
		super();
		this.set(origin, dest, dateTime, hourOfTheDay);
	}

	public void set(String origin, String dest, String dateTime, int hourOfTheDay) {
		this.origin = (origin == null) ? "" : origin;
		this.dest = (dest == null) ? "" : dest;
		this.dateTime = (dateTime == null) ? "" : dateTime;
		this.hourOfTheDay = hourOfTheDay;
	}
	
	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public int getHourOfTheDay() {
		return hourOfTheDay;
	}

	public void setHourOfTheDay(int hourOfTheDay) {
		this.hourOfTheDay = hourOfTheDay;
	}

	public void write(DataOutput out) throws IOException {
		out.writeUTF(origin);
		out.writeUTF(dest);
		out.writeUTF(dateTime);
		out.writeInt(hourOfTheDay);
	}

	public void readFields(DataInput in) throws IOException {
		origin = in.readUTF();
		dest = in.readUTF();
		dateTime = in.readUTF();
		hourOfTheDay = in.readInt();
	}

	public int compareTo(CabRideCompositeKey o) {
		int originCmp = origin.toLowerCase().compareTo(o.origin.toLowerCase());
		if (originCmp != 0) {
			return originCmp;
		} else {
			int destCmp = dest.toLowerCase().compareTo(o.dest.toLowerCase());
			if (destCmp != 0) {
				return destCmp;
			} else {
				int dateTimeCmp = dateTime.toLowerCase().compareTo(o.dateTime.toLowerCase());
				if(dateTimeCmp != 0) {
					return dateTimeCmp;
				}
				else {
					return Integer.compare(hourOfTheDay, o.hourOfTheDay);
				}
			}
		}
	}

}
