package com.xyz.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import com.xyz.util.CabRideCompositeKey;
import com.xyz.util.CabRideDataParser;
import com.xyz.util.CabRideDataPartitioner;
import com.xyz.util.CabRideFullKeyComparator;
import com.xyz.util.CabRideNaturalKeyComparator;

public class CabRideOrderBy {

	public static class CabRideMapper extends Mapper<LongWritable, Text, CabRideCompositeKey, Text> {

		private CabRideCompositeKey compositeKey = new CabRideCompositeKey();
		
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			
			//We skip the first row as it contains header
			if (key.get() == 0 && value.toString().contains("region")) {
                return;
			}
			else {
				//We check for empty or bad records
				if (value != null && !value.toString().trim().equals("") 
						&& value.toString().trim().contains(",")) {
					
					CabRideDataParser.parseData(value.toString());
					
					//If it is NOT a bad record, then only write it
					if(! CabRideDataParser.isBadRecord()) {
						
						String[] origin = CabRideDataParser.getOriginOrDest("Origin");
						String[] dest = CabRideDataParser.getOriginOrDest("Dest");
						String dateTime = CabRideDataParser.getDateTime();
						int hourOfTheDay = CabRideDataParser.getHourOfTheDay();
						
						/*Origin and Destinations are String Arrays. For Proper Comparisons, 
						we write them as String and omit 0th index which is 
						constant (POINT String).  */
						String originForMapper = origin[1]+ "~" + origin[2];
						String destForMapper = dest[1] + "~" + dest[2];
	
						compositeKey.set(originForMapper, destForMapper, dateTime, hourOfTheDay);
						context.write(compositeKey, value);
					}				
				}
			}
		}
	}
	
	public static class CabRideAggregationReducer extends Reducer<CabRideCompositeKey, Text, Text, NullWritable>{
		
		public void reduce(CabRideCompositeKey key, Iterable<Text> values, Context context) throws IOException, InterruptedException{
			
			for(Text t : values) {
				context.write(t, NullWritable.get());
			}
		}
	}
	
	public static void main(String[] args) throws Exception {

		Job job = Job.getInstance(new Configuration(), "Cab Ride Sorting");
		job.setJarByClass(CabRideOrderBy.class);

		// Mapper configuration
		job.setMapperClass(CabRideMapper.class);
		job.setMapOutputKeyClass(CabRideCompositeKey.class);
		job.setMapOutputValueClass(Text.class);

		// Partitioning/Sorting/Grouping configuration
		job.setPartitionerClass(CabRideDataPartitioner.class);
		job.setSortComparatorClass(CabRideFullKeyComparator.class);
		job.setGroupingComparatorClass(CabRideNaturalKeyComparator.class);

		// Reducer configuration
		job.setReducerClass(CabRideAggregationReducer.class);
		
		/*Combiner class is a kind of optimization. Right now commented out because
		there are only 100 entries.  */
		//job.setCombinerClass(CabRideAggregationReducer.class);
		
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(NullWritable.class);
		
		job.setNumReduceTasks(1);
		
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.getLocal(conf);
		
		Path inputPath = fs.makeQualified(new Path(args[0].trim()));  // local path
		FileInputFormat.setInputPaths(job, inputPath);
		
		Path outputPath = fs.makeQualified(new Path(args[1].trim()));  // local path
		FileOutputFormat.setOutputPath(job, outputPath);

		System.exit(job.waitForCompletion(true) ? 0 : 1);

	}
}
